// Position constrains this object to a target
// Usage: Set the ID using the public enumeration

using UnityEngine;
using System.Collections;

public enum NodeType
{
	RightFoot,
	LeftFoot,
	RightKnee,
	LeftKnee,
	Ball,
	Body
}

public class PositionNode : MonoBehaviour {
	
	public NodeType _nodeType;
	
	public Transform target;

	void Update () {
		
		switch (_nodeType)
		{
			case NodeType.RightFoot:
				target = GameObject.FindGameObjectWithTag("RightFoot").transform;
				break;
			case NodeType.LeftFoot:
				target = GameObject.FindGameObjectWithTag("LeftFoot").transform;
				break;
			case NodeType.Body:
				target = GameObject.FindGameObjectWithTag("Body").transform;
				break;
			case NodeType.Ball:
				target = GameObject.FindGameObjectWithTag("MetaTarget").transform;
				break;
			case NodeType.RightKnee:
				target = GameObject.FindGameObjectWithTag("RightKnee").transform;
				break;
			case NodeType.LeftKnee:
				target = GameObject.FindGameObjectWithTag("LeftKnee").transform;
				break;
		}
		
		// Position constrain this object to target
		
		transform.position = target.position;
		
	}
}
