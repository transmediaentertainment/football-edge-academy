using UnityEngine;
using System.Collections;

public class Pinch : MonoBehaviour {
	
	// communicate with camera
	
	public GameObject CameraGameObject;
	public CameraControl CameraControl;
	
	// other parameters
	
	private float pinchMultiplier = 0.1f;
	
	public bool isPinching = false;
	
	void Start ()
	{
		CameraGameObject = GameObject.FindGameObjectWithTag("MainCamera");
		CameraControl = (CameraControl)CameraGameObject.GetComponent("CameraControl");
	}
	
	// Subscribe to events
	void OnEnable(){
		EasyTouch.On_TouchStart2Fingers += On_TouchStart2Fingers;
		EasyTouch.On_PinchIn += On_PinchIn;
		EasyTouch.On_PinchOut += On_PinchOut;
		EasyTouch.On_PinchEnd += On_PinchEnd;
		EasyTouch.On_Cancel2Fingers += On_Cancel2Fingers;
	}

	void OnDisable(){
		UnsubscribeEvent();
	}
	
	void OnDestroy(){
		UnsubscribeEvent();
	}
	
	// Unsubscribe to events
	void UnsubscribeEvent(){
		EasyTouch.On_TouchStart2Fingers -= On_TouchStart2Fingers;
		EasyTouch.On_PinchIn -= On_PinchIn;
		EasyTouch.On_PinchOut -= On_PinchOut;
		EasyTouch.On_PinchEnd -= On_PinchEnd;
		EasyTouch.On_Cancel2Fingers -= On_Cancel2Fingers;
	}
	
	// At the 2 fingers touch beginning
	private void On_TouchStart2Fingers( Gesture gesture){
		
		// disable twist gesture recognize for a real pinch end
		EasyTouch.SetEnableTwist( false);
		EasyTouch.SetEnablePinch( true);
		
		isPinching = true;
		
	}
	
	// At the pinch in
	private void On_PinchIn(Gesture gesture){
	
		//isPinching = true;
			
		float zoom = Time.deltaTime * gesture.deltaPinch;
		
		Vector3 scale = transform.localScale ;
		transform.localScale = new Vector3( scale.x - zoom, scale.y -zoom, scale.z-zoom);
			
		Debug.Log("Delta pinch in : " + gesture.deltaPinch.ToString());
		
		if (CameraControl.cameraFOV < CameraControl.cameraFOVMaximum)
		{
			CameraControl.cameraFOV += (gesture.deltaPinch * pinchMultiplier);
		}
		
		
	}
	
	// At the pinch out
	private void On_PinchOut(Gesture gesture){
		
		//isPinching = true;
			
		float zoom = Time.deltaTime * gesture.deltaPinch;
		
		Vector3  scale = transform.localScale ;
		transform.localScale = new Vector3( scale.x + zoom, scale.y +zoom,scale.z+zoom);
			
		Debug.Log("Delta pinch out : " + gesture.deltaPinch.ToString());
		

		
		if (CameraControl.cameraFOV > CameraControl.cameraFOVMinimum)
		{
			CameraControl.cameraFOV -= (gesture.deltaPinch * pinchMultiplier);
		}

		
	}
	
	// At the pinch end
	private void On_PinchEnd(Gesture gesture){
		
		
		//transform.localScale =new Vector3(1.7f,1.7f,1.7f);
		EasyTouch.SetEnableTwist( true);
		Debug.Log("Pinch me");
		
		isPinching = false;
		
		
	}
	
	
	// If the two finger gesture is finished
	private void On_Cancel2Fingers(Gesture gesture){
		
		/*
		transform.localScale =new Vector3(1.7f,1.7f,1.7f);
		EasyTouch.SetEnableTwist( true);
		textMesh.text="Pinch me";
		*/
	}
	
	
}
