using UnityEngine;
using System.Collections;
using FEA;

public class EffectsControl : MonoBehaviour
{

    [SerializeField]
    private AnimationControl m_AnimationControl;
    [SerializeField]
    private MetaData m_MetaData;
    [SerializeField]
    private GameObject m_AnchorObj;
    [SerializeField]
    private Camera m_UICamera;
    // Camera
    private Camera CameraComponent;

    // Line Renderer
    [SerializeField]
    private LineRenderer lineRendererComponent;

    // Intermediate values for start / end positions for line

    private Vector3 startPosition = new Vector3(0f, 0f, 0f);
    private Vector3 endPosition = new Vector3(0f, 0f, 0f);

    // A Ray used in calculating the start position of the line drawing

    private Ray startRay;

    // slots for nodes

    public GameObject nodeLeftFoot;
    public GameObject nodeRightFoot;
    public GameObject nodeBall;
    public GameObject nodeBody;
    public GameObject nodeRightKnee;
    public GameObject nodeLeftKnee;

    void Start()
    {
        lineRendererComponent.enabled = false;
        // cache camera
        CameraComponent = Camera.main;
    }

    public void TurnOnLineEffect()
    {
        //lineRendererComponent.enabled = true;
        SystemEventCenter.Instance.LateUpdateEvent += UpdatePosition;
    }

    public void TurnOffLineEffect()
    {
        lineRendererComponent.enabled = false;
        SystemEventCenter.Instance.LateUpdateEvent -= UpdatePosition;
    }

    private void UpdatePosition(float deltaTime)
    {
        lineRendererComponent.enabled = true;
        var screenPos = m_UICamera.WorldToScreenPoint(m_AnchorObj.transform.position);
        startRay = CameraComponent.ScreenPointToRay(screenPos);

        startPosition = startRay.origin;

        switch (m_MetaData.GetNodeType(m_AnimationControl.CurrentAnimation, m_AnimationControl.NormalizedTime))
        {
            case CurrentNodeType.LeftFoot:
                endPosition = nodeLeftFoot.transform.position;
                break;
            case CurrentNodeType.RightFoot:
                endPosition = nodeRightFoot.transform.position;
                break;
            case CurrentNodeType.Ball:
                endPosition = nodeBall.transform.position;
                break;
            case CurrentNodeType.Body:
                endPosition = nodeBody.transform.position;
                break;
            case CurrentNodeType.RightKnee:
                endPosition = nodeRightKnee.transform.position;
                break;
            case CurrentNodeType.LeftKnee:
                endPosition = nodeLeftKnee.transform.position;
                break;
        }
        lineRendererComponent.SetPosition(0, startPosition);
        lineRendererComponent.SetPosition(1, endPosition);
    }
}
