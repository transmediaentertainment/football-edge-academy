using UnityEngine;
using System.Collections;

public class CameraFadeHelper : MonoBehaviour {
	
	public GameObject CameraObject;
	public CameraFade CameraFadeClass;
	
	public GUIStyle guiStyle;

	void Start () {
		
		CameraObject = GameObject.FindGameObjectWithTag("MainCamera");
		CameraFadeClass = (CameraFade)CameraObject.GetComponent("CameraFade");
		guiStyle = CameraFadeClass.m_BackgroundStyle;
	
	}
	
	void Update () {
	
	}
}
