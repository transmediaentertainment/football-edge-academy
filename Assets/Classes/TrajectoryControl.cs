using UnityEngine;
using System.Collections;

public class TrajectoryControl : MonoBehaviour 
{
	private TrailRenderer TrailRendererComponent;
    public AnimationControl m_AnimControl;
    public AnimationSceneUIController m_UIController;

    private float m_ExtraTime = 0f;
    private const float m_StandardDisplayTime = 1f;

	void Start () 
    {
		TrailRendererComponent = this.GetComponent<TrailRenderer>();
        TrailRendererComponent.time = m_StandardDisplayTime;
	}

	void Update () 
    {
        if (m_AnimControl == null)
            return;
		// reset trail at beginning and end of animation loops
		if(m_AnimControl.NormalizedTime > 0.95f || m_AnimControl.NormalizedTime < 0.03f)
		{
			TrailRendererComponent.time = 0f;
            m_ExtraTime = 0f;
		}
		else
		{
            m_ExtraTime += Time.deltaTime * (1f - SpeedControl.Instance.Speed);
            TrailRendererComponent.time = m_StandardDisplayTime + m_ExtraTime;
		}
	}
}
