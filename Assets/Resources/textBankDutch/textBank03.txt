Loop direkt achter de bal in dezelfde richting
Stap uit en plaats je linker voet aan de linker kant van de bal
Gebruik de binnenkant van je rechter voet, tik de bal naar binnen achter je linker voet
Terwijl de bal achter je linker voet rolt, verander de richting van de bal naar de linker kant
