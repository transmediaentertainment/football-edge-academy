Conduis le ballon droit devant
Avec la pointe du pied droit, pousse le ballon sur la droite
Prends appui sur la jambe gauche
Etends ta jambe droite, bout du pied à hauteur du ballon
Crochète le ballon de l'intérieur du pied droit et ramène-le côté gauche
