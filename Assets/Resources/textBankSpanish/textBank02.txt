Correr en línea con el balón
Dar un paso y colocar el pie izquierdo detrás del balón 
Pasar el peso del cuerpo al pie izquierdo doblando ligeramente la rodilla
Iniciar un giro de 360 grados, colocando el pie derecho sobre el balón a los 45 grados del giro
Aproximadamente a los 180 grados del giro colocar el pie izquierdo sobre el balón
Continuar rotando hasta que se tenga el balón al frente completando los 360 grados
