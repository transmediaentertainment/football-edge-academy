Bring your right foot out and around to the front of the ball
With your right foot in front of the ball and not yet on the ground, hop and turn your left foot 90 degrees so that the inside of your left foot faces the ball
Still in the air tap the ball with the back of your right foot (with your heel)
The ball will roll back and rebound off the inside of your left foot and go forward. The speed will depend how hard you first hit the ball with your heel
