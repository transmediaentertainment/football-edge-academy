Place your right foot on top of the ball with your toe of the boot facing forward
Pull the ball directly back behind you and use the heel of your right foot to push the ball behind your left foot
Turn your body to the left, it should be around 90 degrees of a turn and your ball will be rolling forward ready for your chase
