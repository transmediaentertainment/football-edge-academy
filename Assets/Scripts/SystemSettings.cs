using UnityEngine;
using FEA.Language;

namespace FEA
{
    public enum IPhoneGen
    {
        Third,
        Fourth,
        Fifth
    }

    public class SystemSettings
    {
        public bool SwitchedLanguage { get; set; }

        public bool FirstRun { get; set; }

        public IPhoneGen iPhoneGen { get; set; }

        private static SystemSettings m_Instance;

        public static SystemSettings Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new SystemSettings();
                }
                return m_Instance;
            }
        }

        private SystemSettings()
        {
            FirstRun = true;
            
            Screen.autorotateToLandscapeLeft = true;
            Screen.autorotateToLandscapeRight = true;
            Screen.autorotateToPortrait = false;
            Screen.autorotateToPortraitUpsideDown = false;
            Screen.orientation = ScreenOrientation.AutoRotation;

            // TODO : replace this with some proper code.
            if (Screen.width == 1136)
                iPhoneGen = IPhoneGen.Fifth;
            else if (Screen.width == 960)
                iPhoneGen = IPhoneGen.Fourth;
            else if (Screen.width == 480)
                iPhoneGen = IPhoneGen.Third;
        }
    }
}

