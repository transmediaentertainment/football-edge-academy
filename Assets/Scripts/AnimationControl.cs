using UnityEngine;
using System.Collections;
using TransTech.System.Debug;

public class AnimationControl : MonoBehaviour 
{
    // NOTE : Must be initialized in the Unity Editor
    [SerializeField]
    private string[] m_AnimationList;

    [SerializeField]
    private CameraControl m_CameraControl;

    [SerializeField]
    private AnimationSceneUIController m_UIController;

    public int CurrentAnimation { get; private set; }

    private const int AnimationCount = 15;

    private const float DefaultSpeed = 1f;
    private const float SlowMotionSpeed = 0.33f;
    private const float PauseSpeed = 0f;

    private const float SlowTime = 0.25f;
    private float m_StartSlowSpeed;
    private float m_SlowTimer;
    private bool m_IsSlowing;

    public float NormalizedTime { get { return animation[m_AnimationList[CurrentAnimation]].normalizedTime; } }

    private void Awake()
    {
        if (m_AnimationList == null || m_AnimationList.Length != AnimationCount)
        {
            TTDebug.LogError("AnimationControl.cs : Animations have not been setup correctly in the Unity Editor.");
        }

        SpeedControl.Instance.SpeedChangedEvent += SpeedChanged;
        m_UIController.ActionButtonPressedEvent += SetAnimation;
    }

    private void OnDestroy()
    {
        SpeedControl.Instance.SpeedChangedEvent -= SpeedChanged;
		
		if( m_IsRegistered )
		{
			CameraFadeFSM.FadeOutCompleteEvent -= RewindAndResetParameters;
			m_IsRegistered = false;
		}
    }

    public void SetAnimation(int anim)
    {
        if (anim < 0 || anim >= AnimationCount)
        {
            TTDebug.LogError("AnimationControl.cs : Invalid animation index : " + anim);
            return;
        }

        if (anim == CurrentAnimation)
        {
            // Already on this animation
            return;
        }

        CurrentAnimation = anim;
		m_CameraControl.FadeOut();
		RegisterForFadeOut();
		
        //animation.CrossFade(m_AnimationList[CurrentAnimation], 0.01f);
    }
	
	private bool m_IsRegistered = false;
	
	private void RegisterForFadeOut()
	{
		if(!m_IsRegistered)
		{
			CameraFadeFSM.FadeOutCompleteEvent += RewindAndResetParameters;
			m_IsRegistered = true;
		}
	}
	
    private void Update()
    {
        /*if (animation[m_AnimationList[CurrentAnimation]].normalizedTime >= 1f)
        {
            RewindAndResetParameters();
        }*/

        if (animation[m_AnimationList[CurrentAnimation]].time  >= animation[m_AnimationList[CurrentAnimation]].length - (CameraControl.CameraFadeDuration * SpeedControl.Instance.Speed))
        {
            m_CameraControl.FadeOut();
			RegisterForFadeOut();
        }
    }

    public void RewindAndResetParameters()
    {
		animation.Play(m_AnimationList[CurrentAnimation]);
		animation.Rewind();
		//animation.CrossFade(m_AnimationList[CurrentAnimation], 0.01f);
        //animation.Rewind();
        //m_CameraControl.FadeIn();
		CameraFadeFSM.FadeOutCompleteEvent -= RewindAndResetParameters;
		m_IsRegistered = false;
    }

    private void SpeedChanged(float newSpeed)
    {
        if (newSpeed <= 0.01f)
            Pause();
        else if (newSpeed >= 0.99f)
            PlayFullSpeed();
        else
            PlaySlowMo();
    }

    public void PlayFullSpeed()
    {
        // We need to cancel any slow down.  If we don't, the slow down can override the new speed setting.
		CancelSlowAnimation();
        animation[m_AnimationList[CurrentAnimation]].speed = DefaultSpeed;
    }

    public void PlaySlowMo()
    {
        // We need to cancel any slow down.  If we don't, the slow down can override the new speed setting.
		CancelSlowAnimation();
        animation[m_AnimationList[CurrentAnimation]].speed = SlowMotionSpeed; 
    }

    public void Pause()
    {
        if (m_IsSlowing)
            return;
        m_StartSlowSpeed = animation[m_AnimationList[CurrentAnimation]].speed;
        m_SlowTimer = 0f;
        m_IsSlowing = true;
        SystemEventCenter.Instance.UpdateEvent += SlowAnimation;
    }

    private void SlowAnimation(float deltaTime)
    {
        m_SlowTimer += deltaTime;
        animation[m_AnimationList[CurrentAnimation]].speed = Mathf.Lerp(m_StartSlowSpeed, PauseSpeed, Mathf.Clamp01(m_SlowTimer / SlowTime));
        if (m_SlowTimer >= SlowTime)
        {
            CancelSlowAnimation();
        }
    }
	
	private void CancelSlowAnimation()
	{
		if(!m_IsSlowing)
			return;
		
		m_IsSlowing = false;
		SystemEventCenter.Instance.UpdateEvent -= SlowAnimation;
	}
}

