using TransTech.Delegates;
using TransTech.System.Debug;

public class SpeedControl
{

    private static SpeedControl m_Instance;
    public static SpeedControl Instance
    {
        get
        {
            if (m_Instance == null)
                m_Instance = new SpeedControl();
            return m_Instance;
        }
    }

    private float m_Speed;
    public float Speed
    { 
        get 
        { 
            return m_Speed; 
        } 
        private set
        {
            m_Speed = value;
            if(SpeedChangedEvent != null)
                SpeedChangedEvent(m_Speed);
        }
    }
    public event FloatDelegate SpeedChangedEvent;

    private SpeedControl()
    {
        SetFullSpeed();
    }

    public void SetSlowMotion()
    {
        Speed = 0.33f;
    }

    public void SetPaused()
    {
        Speed = 0f;
    }

    public void SetFullSpeed()
    {
        Speed = 1f;
    }
}
