using UnityEngine;
using FEA.Language;
using FEA.UI;

[RequireComponent(typeof(UISprite))]
public class UISpriteLanguageSet : MonoBehaviour
{
	[SerializeField]
	private string m_EnglishUKSprite;
	[SerializeField]
	private string m_EnglishUSSprite;
	[SerializeField]
	private string m_FrenchSprite;
	[SerializeField]
	private string m_SpanishSprite;
	[SerializeField]
	private string m_PortugueseSprite;
	
	[SerializeField]
	private UISprite m_Sprite;
	
	private void Awake()
	{
		m_Sprite = GetComponent<UISprite>();
		UpdateLanguage();
		LanguageController.Instance.NewLanguageSetEvent += UpdateLanguage;
	}
	
	private void UpdateLanguage()
	{
		switch(LanguageController.Instance.CurrentLanguage)
		{
		case LanguageType.EnglishUK:
			m_Sprite.spriteName = m_EnglishUKSprite;
			break;
		case LanguageType.EnglishUS:
			m_Sprite.spriteName = m_EnglishUSSprite;
			break;
		case LanguageType.French:
			m_Sprite.spriteName = m_FrenchSprite;
			break;
		case LanguageType.Portugues:
			m_Sprite.spriteName = m_PortugueseSprite;
			break;
		case LanguageType.Spanish:
			m_Sprite.spriteName = m_SpanishSprite;
			break;
		}
	}
	
	private void OnDestroy()
	{
		LanguageController.Instance.NewLanguageSetEvent -= UpdateLanguage;
	}
}


