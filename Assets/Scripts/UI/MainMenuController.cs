using UnityEngine;
using System.Collections;
using FEA.Language;
using Holoville.HOTween;

namespace FEA.UI
{
    public class MainMenuController : MonoBehaviour
    {
        private readonly Vector3 m_MainPanelOffScreenPos = new Vector3(-1300f, 0f, 0f);
        private readonly Vector3 m_LanguagePanelOffScreenPos = new Vector3(1300f, 0f, 0f);
        private readonly Vector3 m_OnScreenPanelPos = new Vector3(0f, 0f, 0f);
        private const float m_TweenTime = 0.5f;

        private const float m_AudioClipTime = 0.504f;

        public UIPanel m_MainMenuPanel;
        public UIPanel m_LanguagePanel;
		public UIPanel m_CreditsPanel;
		public UIPanel m_LinksPanel;
		
		// To highlight Lang buttons
		public UIButton m_LangDevicesButton;
		public UIButton m_LangEnglishButton;
		public UIButton m_LangEnglishUSButton;
		public UIButton m_LangFrenchButton;
		public UIButton m_LangSpanishButton;
		public UIButton m_LangPortuguesButton;
		
		public UIButton[] m_LangButtons;
		
		public Color m_DefaultButtonColor;
		public Color m_SelectedDefaultButtonColor;

        public UIFont m_Font;

        private void Awake()
        {
			// Unlock FPS to 60
			Application.targetFrameRate = 60;
			
			// To dehighlight Lang buttons
			m_LangButtons = new UIButton[] { m_LangDevicesButton, m_LangEnglishButton, m_LangEnglishUSButton, m_LangFrenchButton, m_LangSpanishButton, m_LangPortuguesButton };
			
            if (SystemSettings.Instance.FirstRun)
            {
                Handheld.PlayFullScreenMovie("Intro-converted.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
                SystemSettings.Instance.FirstRun = false;
				AnimateMainPanelOn();
            }
			else
			{
				AnimateMainPanelOn(true);
			}
			
			// Set language when scene loads
			SetButtonColor( m_DefaultButtonColor, m_LangButtons );
			
			if( LanguageController.Instance.DeviceLanguage == 1 )
			{
				SetButtonColor( m_SelectedDefaultButtonColor, m_LangDevicesButton );
			}
			else
			{
				switch( LanguageController.Instance.CurrentLanguage )
				{
					case LanguageType.EnglishUK:
					{
						SetButtonColor( m_SelectedDefaultButtonColor, m_LangEnglishButton );
						break;
					}
					case LanguageType.EnglishUS:
					{
						SetButtonColor( m_SelectedDefaultButtonColor, m_LangEnglishUSButton );
						break;
					}
					case LanguageType.French:
					{
						SetButtonColor( m_SelectedDefaultButtonColor, m_LangFrenchButton );
						break;
					}
					case LanguageType.Spanish:
					{
						SetButtonColor( m_SelectedDefaultButtonColor, m_LangSpanishButton );
						break;
					}
					case LanguageType.Portugues:
					{
						SetButtonColor( m_SelectedDefaultButtonColor, m_LangPortuguesButton );
						break;
					}
				}
			}
        }

        private void TrainButtonPressed()
        {
            StartCoroutine(LoadAfterSoundPlays());
        }

        private IEnumerator LoadAfterSoundPlays()
        {
            yield return new WaitForSeconds(m_AudioClipTime);
            Application.LoadLevel("NGUIScene");
        }

        private void LanguageButtonPressed()
        {
			AnimateMainPanelOff();
			AnimatePanel(m_LanguagePanel.transform, m_OnScreenPanelPos);
        }
		
		private void CreditsButtonPressed()
		{
			AnimateMainPanelOff();
			AnimatePanel(m_CreditsPanel.transform, m_OnScreenPanelPos);
		}
		
		private void CreditsBackButtonPressed()
		{
			AnimateMainPanelOn();
			AnimatePanel(m_CreditsPanel.transform, m_LanguagePanelOffScreenPos);
		}
		
		private void LinksButtonPressed()
		{
			AnimateMainPanelOff();
			AnimatePanel(m_LinksPanel.transform, m_OnScreenPanelPos);
		}
		
		private void DeviceButtonPressed()
		{
			SetLanguageAndReturn(LanguageType.NotSet);
			SetButtonColor( m_SelectedDefaultButtonColor, m_LangDevicesButton );
		}
		
		private void EnglishButtonPressed()
        {
            SetLanguageAndReturn(LanguageType.EnglishUK);
			SetButtonColor( m_SelectedDefaultButtonColor, m_LangEnglishButton );
        }

        private void EnglishUSButtonPressed()
        {
            SetLanguageAndReturn(LanguageType.EnglishUS);
			SetButtonColor( m_SelectedDefaultButtonColor, m_LangEnglishUSButton );
        }

        private void FrenchButtonPressed()
        {
            SetLanguageAndReturn(LanguageType.French);
			SetButtonColor( m_SelectedDefaultButtonColor, m_LangFrenchButton );
        }

        private void SpanishButtonPressed()
        {
            SetLanguageAndReturn(LanguageType.Spanish);
			SetButtonColor( m_SelectedDefaultButtonColor, m_LangSpanishButton );
        }

        private void PortugeseButtonPressed()
        {
            SetLanguageAndReturn(LanguageType.Portugues);
			SetButtonColor( m_SelectedDefaultButtonColor, m_LangPortuguesButton );
        }

        private void SetLanguageAndReturn(LanguageType language)
        {
			SetButtonColor( m_DefaultButtonColor, m_LangButtons );
            LanguageController.Instance.SetLanguage(language);
			AnimatePanel(m_LanguagePanel.transform, m_LanguagePanelOffScreenPos);
			AnimateMainPanelOn();
        }

        private void LanguageBackButtonPressed()
        {
			AnimatePanel(m_LanguagePanel.transform, m_LanguagePanelOffScreenPos);
			AnimateMainPanelOn();
        }
		
		private void MEASiteButtonPressed()
		{
			Application.OpenURL("http://motionedgeacademy.com/");
		}
		
		private void SportzAppzFBButtonPressed()
		{
            // NOTE: This can probably be done on iOS as well.  Will need to test when updating that project. - JM
#if UNITY_ANDROID
            Application.OpenURL("fb://page/497361373652471");
#else
			Application.OpenURL("https://www.facebook.com/FootballEdgeAcademy");
#endif
		}
		
		private void TransmediaSiteButtonPressed()
		{
			Application.OpenURL("http://transmedia-entertainment.com/");
		}
		
		private void LinksBackButtonPressed()
		{
			AnimateMainPanelOn();
			AnimatePanel(m_LinksPanel.transform, m_LanguagePanelOffScreenPos);
		}
		
		private void AnimateMainPanelOn()
		{
			AnimateMainPanelOn(false);
		}
		
		private void AnimateMainPanelOn( bool instant )
		{
			AnimatePanel(m_MainMenuPanel.transform, m_OnScreenPanelPos, instant);
		}
		
		private void AnimateMainPanelOff()
		{
			AnimatePanel(m_MainMenuPanel.transform, m_MainPanelOffScreenPos, false);
		}
		
		private void AnimatePanel(Transform panel, Vector3 endPos)
		{
			AnimatePanel(panel, endPos, false);
		}
		
		private void AnimatePanel(Transform panel, Vector3 endPos, bool instant)	
		{
			HOTween.To(panel, instant ? m_TweenTime : 0.01f, new TweenParms().Prop("localPosition", endPos).Ease(EaseType.EaseInOutQuad));
		}
		
		private void SetButtonColor( Color defaultColor, params UIButton[] buttons )
		{
			foreach( UIButton button in buttons )
			{
				button.defaultColor = defaultColor;
				
				button.UpdateColor(true, false);
			}
		}
    }
}