using UnityEngine;
using System.Collections;
using TransTech.Delegates;
using TransTech.Delegates.Unity;
using FEA.Language;

public class PlayStateUI
{
    public UISprite PausedSprite { get; set; }
    public UILabel PausedLabel { get; set; }
    public UISprite SlowMoSprite { get; set; }
    public UILabel SlowMoLabel { get; set; }
    public UISprite StepSprite { get; set; }
    public UILabel StepLabel { get; set; }

    public PlayStateUI(UISprite pausedSprite, UILabel pausedLabel, UISprite slowMoSprite, UILabel slowMoLabel, UISprite stepSprite, UILabel stepLabel)
    {
        PausedSprite = pausedSprite;
        PausedLabel = pausedLabel;
        SlowMoSprite = slowMoSprite;
        SlowMoLabel = slowMoLabel;
        StepSprite = stepSprite;
        StepLabel = stepLabel;
    }
}

public class AnimationSceneUIController : MonoBehaviour
{
    [SerializeField]
    private UIPanel m_MainCameraPanel;
    [SerializeField]
    private UIPanel m_SmallCameraPanel;

    [SerializeField]
    private UIButton m_CameraHighButton;
    [SerializeField]
    private UIButton m_CameraMidButton;
    [SerializeField]
    private UIButton m_CameraLowButton;

    [SerializeField]
    private UIButton m_CameraNorthButton;
    [SerializeField]
    private UIButton m_CameraSouthButton;
    [SerializeField]
    private UIButton m_CameraEastButton;
    [SerializeField]
    private UIButton m_CameraWestButton;

    [SerializeField]
    private UIImageButton m_PlayButton;
    [SerializeField]
    private UIImageButton m_PauseButton;
    [SerializeField]
    private UIImageButton m_StepButton;
    [SerializeField]
    private UIImageButton m_SlowMoButton;

    [SerializeField]
    private UISprite m_SlowMoCamIcon;
    [SerializeField]
    private UISprite m_PauseCamIcon;
    [SerializeField]
    private UISprite m_StepCamIcon;
    [SerializeField]
    private UILabel m_SlowMoCamLabel;
    [SerializeField]
    private UILabel m_PauseCamLabel;
    [SerializeField]
    private UILabel m_StepCamLabel;

    [SerializeField]
    private UIPanel m_MainActionsPanel;
    [SerializeField]
    private UIPanel m_SmallActionsPanel;
    [SerializeField]
    private UILabel m_InfoLabel;
    [SerializeField]
    private UILabel m_StepInfoLabel;
    [SerializeField]
    private UILabel m_VerticalLabel;

    [SerializeField]
    private MetaData m_MetaData;
    [SerializeField]
    private AnimationControl m_PlayerAnimationControl;

    [SerializeField]
    private EffectsControl m_EffectsControl;
    [SerializeField]
    private CameraFade m_CameraFade;
    [SerializeField]
    private UIPanel m_TablePanel;

    [SerializeField]
    private UIButton[] m_ActionListButtons;

    //private CameraUIFSMEngine m_CameraUIFSM;
    //private ActionListUIFSMEngine m_ActionUIFSM;
    //private AnimationControlUIFSMEngine m_AnimControlUIFSM;
    private PlayStateUI m_PlayStateUI;

    void Awake()
    {
        m_PlayStateUI = new PlayStateUI(m_PauseCamIcon, m_PauseCamLabel, m_SlowMoCamIcon, m_SlowMoCamLabel, m_StepCamIcon, m_StepCamLabel);
        /*m_CameraUIFSM = */new CameraUIFSMEngine(this, m_MainCameraPanel, m_SmallCameraPanel);
        /*m_ActionUIFSM = */new ActionListUIFSMEngine(this, m_MetaData, m_MainActionsPanel, m_SmallActionsPanel, m_InfoLabel, m_StepInfoLabel);
        /*m_AnimControlUIFSM = */new AnimationControlUIFSMEngine(this, m_MetaData, m_PlayerAnimationControl, m_PlayButton, m_PauseButton, m_SlowMoButton, m_StepButton, m_PlayStateUI, m_StepInfoLabel, m_EffectsControl);

        m_VerticalLabel.text = LanguageController.Instance.GetValueForKey("FakeChange");
        var localPos = m_VerticalLabel.transform.localPosition;
        localPos.z = -0.5f;
        m_VerticalLabel.transform.localPosition = localPos;
        // This is how we are highlighting the buttons.
        m_ActionListButtons[0].isEnabled = false;

        UICamera.fallThrough = gameObject;
    }

    #region Button Pressed Functions And Events
    // Camera Panel events
    public event VoidDelegate CameraPanelCrossPressedEvent;
    public event VoidDelegate CameraPanelHighPressedEvent;
    public event VoidDelegate CameraPanelMidPressedEvent;
    public event VoidDelegate CameraPanelLowPressedEvent;
    public event VoidDelegate CameraPanelNorthPressedEvent;
    public event VoidDelegate CameraPanelSouthPressedEvent;
    public event VoidDelegate CameraPanelEastPressedEvent;
    public event VoidDelegate CameraPanelWestPressedEvent;
    public event VoidDelegate ShowCameraPanelPressedEvent;

    // Camera Panel Functions
    private void CameraPanelCrossPressed() { if (CameraPanelCrossPressedEvent != null) CameraPanelCrossPressedEvent(); }
    private void CameraPanelHighPressed()
    { 
        if (CameraPanelHighPressedEvent != null)
            CameraPanelHighPressedEvent();

        SetCameraHeightButtonsHighlight(true, false, false);
    }

    private void CameraPanelMidPressed() 
    {
        if (CameraPanelMidPressedEvent != null)
            CameraPanelMidPressedEvent();

        SetCameraHeightButtonsHighlight(false, true, false);
    }

    private void CameraPanelLowPressed() 
    { 
        if (CameraPanelLowPressedEvent != null)
            CameraPanelLowPressedEvent();

        SetCameraHeightButtonsHighlight(false, false, true);
    }

    private void CameraPanelNorthPressed() 
    { 
        if (CameraPanelNorthPressedEvent != null) 
            CameraPanelNorthPressedEvent();

        SetCameraDirectionButtonsHighlight(true, false, false, false);
    }

    private void CameraPanelSouthPressed() 
    { 
        if (CameraPanelSouthPressedEvent != null) 
            CameraPanelSouthPressedEvent();

        SetCameraDirectionButtonsHighlight(false, true, false, false);
    }

    private void CameraPanelEastPressed() 
    { 
        if (CameraPanelEastPressedEvent != null) 
            CameraPanelEastPressedEvent();

        SetCameraDirectionButtonsHighlight(false, false, true, false);
    }

    private void CameraPanelWestPressed() 
    { 
        if (CameraPanelWestPressedEvent != null) 
            CameraPanelWestPressedEvent();

        SetCameraDirectionButtonsHighlight(false, false, false, true);
    }

    private void ShowCameraPanelPressed() { if (ShowCameraPanelPressedEvent != null) ShowCameraPanelPressedEvent(); }

    // Actions Panel Events
    public event VoidDelegate ShowActionPanelPressedEvent;
    public event VoidDelegate ShowInfoButtonPressedEvent;

    // Actions Panel Functions
    private void ShowActionPanelPressed() { if (ShowActionPanelPressedEvent != null) ShowActionPanelPressedEvent(); }
    private void ShowInfoButtonPressed() { if (ShowInfoButtonPressedEvent != null) ShowInfoButtonPressedEvent(); }

    // Static Button Events
    //public event VoidDelegate MainMenuButtonPressedEvent;

    // Static Button Functions
    private void MainMenuButtonPressed() 
    {
        StartCoroutine(LoadMainMenuAfterPause());        
    }

    private IEnumerator LoadMainMenuAfterPause()
    {
        // Wait for the sound effect to play
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel("MainMenu"); 
    }

    // Play control Events
    public event VoidDelegate PlayButtonPressedEvent;
    public event VoidDelegate PauseButtonPressedEvent;
    public event VoidDelegate SlowMoButtonPressedEvent;
    public event VoidDelegate StepButtonPressedEvent;

    // Play Control Functions
    private void PlayButtonPressed() { if (!m_CameraFade.IsFading && PlayButtonPressedEvent != null) PlayButtonPressedEvent(); }
    private void PauseButtonPressed() { if (!m_CameraFade.IsFading && PauseButtonPressedEvent != null) PauseButtonPressedEvent(); }
    private void SlowMoButtonPressed() { if (!m_CameraFade.IsFading && SlowMoButtonPressedEvent != null) SlowMoButtonPressedEvent(); }
    private void StepButtonPressed() { if (!m_CameraFade.IsFading && StepButtonPressedEvent != null) { StepButtonPressedEvent(); } }

    // Action button events
    public event IntDelegate ActionButtonPressedEvent;

    // Action button functions
    private void ActionButtonPressed(GameObject go)
    {
        var splits = go.name.Split(' ');
        int num;
        if (int.TryParse(splits[0], out num))
        {
            if(ActionButtonPressedEvent != null)
                ActionButtonPressedEvent(num);

            for (int i = 0; i < m_ActionListButtons.Length; i++)
            {
                m_ActionListButtons[i].isEnabled = i != num;
            }
        }

        var lang = go.GetComponentInChildren<UILabelLanguageSet>();
        m_VerticalLabel.text = LanguageController.Instance.GetValueForKey(lang.LabelKey);
    }

    public static event Vector2Delegate FallthroughDragEvent;

    // Catches drag events not used by UICamera
    private void OnDrag(Vector2 vec)
    {
        if (FallthroughDragEvent != null)
            FallthroughDragEvent(vec);
    }

    #endregion

    private void SetCameraHeightButtonsHighlight(bool high, bool mid, bool low)
    {
        m_CameraHighButton.isEnabled = !high;
        m_CameraMidButton.isEnabled = !mid;
        m_CameraLowButton.isEnabled = !low;

        if(!m_CamHeightRegistered)
        {
            FallthroughDragEvent += CameraHeightButtonSwipe;
            m_CamHeightRegistered = true;
        }
    }

    private bool m_CamHeightRegistered = false;
    private void CameraHeightButtonSwipe(Vector2 vec)
    {
        if (vec.y != 0f)
        {
            m_CameraHighButton.isEnabled = true;
            m_CameraMidButton.isEnabled = true;
            m_CameraLowButton.isEnabled = true;
            FallthroughDragEvent -= CameraHeightButtonSwipe;
            m_CamHeightRegistered = false;
        }
    }

    private void SetCameraDirectionButtonsHighlight(bool north, bool south, bool east, bool west)
    {
        m_CameraNorthButton.isEnabled = !north;
        m_CameraSouthButton.isEnabled = !south;
        m_CameraEastButton.isEnabled = !east;
        m_CameraWestButton.isEnabled = !west;

        if (!m_CamDirectionRegistered)
        {
            FallthroughDragEvent += CamDirectionButtonSwipe;
            m_CamDirectionRegistered = true;
        }
    }

    private bool m_CamDirectionRegistered = false;
    private void CamDirectionButtonSwipe(Vector2 vec)
    {
        if (vec.x != 0f)
        {
            m_CameraNorthButton.isEnabled = true;
            m_CameraSouthButton.isEnabled = true;
            m_CameraEastButton.isEnabled = true;
            m_CameraWestButton.isEnabled = true;
            FallthroughDragEvent -= CamDirectionButtonSwipe;
            m_CamDirectionRegistered = false;
        }
    }
	
	public void OnDestroy()
	{
		if( m_CamHeightRegistered )
		{
			FallthroughDragEvent -= CameraHeightButtonSwipe;
			m_CamHeightRegistered = false;
		}
		if( m_CamDirectionRegistered )
		{
			FallthroughDragEvent -= CamDirectionButtonSwipe;
			m_CamDirectionRegistered = false;
		}
	}

}
