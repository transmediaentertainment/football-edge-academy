using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public abstract class CameraUIState : FSMState
{
    protected CameraUIFSMEngine m_FSM;

    protected UIPanel m_MainPanel;
    protected UIPanel m_SmallPanel;

    public CameraUIState(CameraUIFSMEngine fsm, UIPanel mainPanel, UIPanel smallPanel)
    {
        m_FSM = fsm;
        m_MainPanel = mainPanel;
        m_SmallPanel = smallPanel;
    }
}
