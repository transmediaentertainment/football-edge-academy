using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class CameraUIFSMEngine : FSM 
{
    private AnimationSceneUIController m_Controller;
    private CameraPanelHiddenState m_HiddenState;
    private CameraPanelShowingState m_ShowingState;

    public Vector3 MainPanelShowingPos { get; private set; }
    public Vector3 MainPanelHidingPos { get; private set; }
    public Vector3 SmallPanelShowingPos { get; private set; }
    public Vector3 SmallPanelHidingPos { get; private set; }

    public const float TweenPositionTime = 0.25f;

    public CameraUIFSMEngine(AnimationSceneUIController controller, UIPanel mainPanel, UIPanel smallPanel)
    {
        m_Controller = controller;
        m_HiddenState = new CameraPanelHiddenState(this, mainPanel, smallPanel);
        m_ShowingState = new CameraPanelShowingState(this, mainPanel, smallPanel);

        MainPanelShowingPos = new Vector3(-133f, 0f, 0f);
        MainPanelHidingPos = new Vector3(165f, 0f, 0f);

        SmallPanelShowingPos = new Vector3(-37f, 0f, 0f);
        SmallPanelHidingPos = new Vector3(37f, 0f, 0f);

        // Force into position
        mainPanel.transform.localPosition = MainPanelHidingPos;
        smallPanel.transform.localPosition = SmallPanelHidingPos;

        // Register for events
        m_Controller.CameraPanelCrossPressedEvent += () => NewState(m_HiddenState);
        m_Controller.ShowCameraPanelPressedEvent += () => NewState(m_ShowingState);

        NewState(m_ShowingState);
    }
}
