using TransTech.FiniteStateMachine;
using Holoville.HOTween;

public class CameraPanelShowingState : CameraUIState
{

    public CameraPanelShowingState(CameraUIFSMEngine fsm, UIPanel mainPanel, UIPanel smallPanel)
        : base(fsm, mainPanel, smallPanel)
    {
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        HOTween.To(m_MainPanel.transform, CameraUIFSMEngine.TweenPositionTime, new TweenParms().Prop("localPosition", m_FSM.MainPanelShowingPos).Ease(EaseType.EaseInOutQuad));
        HOTween.To(m_SmallPanel.transform, CameraUIFSMEngine.TweenPositionTime, new TweenParms().Prop("localPosition", m_FSM.SmallPanelHidingPos).Ease(EaseType.EaseInOutQuad));
    
    }
}
