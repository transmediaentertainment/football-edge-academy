using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class ActionListUIFSMEngine : FSM
{
    private AnimationSceneUIController m_Controller;
    private UIPanel m_MainPanel;
    private UIPanel m_SmallPanel;
    private UILabel m_InfoLabel;
    private UILabel m_StepInfoLabel;

    private ActionListShowingState m_ShowingState;
    private ActionListHiddenState m_HiddenState;
    private AdditionalInfoShowingState m_InfoState;

    public Vector3 MainPanelShowingPos { get; private set; }
    public Vector3 MainPanelHidingPos { get; private set; }
    public Vector3 SmallPanelShowingPos { get; private set; }
    public Vector3 SmallPanelHidingPos { get; private set; }

    public const float TweenDuration = 0.25f;

    public ActionListUIFSMEngine(AnimationSceneUIController controller, MetaData metaData, UIPanel mainPanel, UIPanel smallPanel, UILabel infoLabel, UILabel stepInfoLabel)
    {
        // Cache items
        m_Controller = controller;
        m_MainPanel = mainPanel;
        m_SmallPanel = smallPanel;
        m_InfoLabel = infoLabel;
        m_StepInfoLabel = stepInfoLabel;

        // Create States
        m_ShowingState = new ActionListShowingState(controller, this, m_MainPanel, m_SmallPanel);
        m_HiddenState = new ActionListHiddenState(this, m_MainPanel, m_SmallPanel);
        m_InfoState = new AdditionalInfoShowingState(controller, this, metaData, m_InfoLabel);

        // Setup positions
        MainPanelShowingPos = new Vector3(212f, 0f, 0f);
        MainPanelHidingPos = new Vector3(-220f, 0f, 0f);

        SmallPanelShowingPos = new Vector3(40f, 0f, 0f);
        SmallPanelHidingPos = new Vector3(-395f, 0f, 0f);

        // Force into position
        m_MainPanel.transform.localPosition = MainPanelHidingPos;
        m_SmallPanel.transform.localPosition = SmallPanelHidingPos;

        m_StepInfoLabel.gameObject.SetActive(false);
        m_InfoLabel.gameObject.SetActive(false);

        // register for events
        m_ShowingState.TimerFinishedEvent += SetHiddenState;
        m_Controller.ShowActionPanelPressedEvent += () => NewState(m_ShowingState);
        m_Controller.ShowInfoButtonPressedEvent += () => NewState(m_InfoState);
        // If we go into step mode, we want to hide
        m_Controller.StepButtonPressedEvent += () => { if (Peek() == m_ShowingState) SetHiddenState(); };
        //m_Controller.FallthroughDragEvent += (v) => SetHiddenState();

        NewState(m_ShowingState);
    }

    public void SetHiddenStateWrapper(Vector2 vec)
    {
        SetHiddenState();
    }

    public void SetHiddenState()
    {
        if(Peek() != m_HiddenState)
            NewState(m_HiddenState);
    }
}
