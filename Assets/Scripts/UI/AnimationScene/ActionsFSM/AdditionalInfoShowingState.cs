using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class AdditionalInfoShowingState : FSMState
{
    private AnimationSceneUIController m_UIController;
    private ActionListUIFSMEngine m_FSM;
    private MetaData m_MetaData;
    private UILabel m_InfoLabel;

    private string m_FullText;
    private float m_Timer;
    private int m_CurrentIndex;
    private const float m_NewCharTime = 0.05f;

    public AdditionalInfoShowingState(AnimationSceneUIController uiController, ActionListUIFSMEngine fsm, MetaData metaData, UILabel infoLabel)
    {
        m_UIController = uiController;
        m_FSM = fsm;
        m_MetaData = metaData;
        m_InfoLabel = infoLabel;
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        m_InfoLabel.gameObject.SetActive(true);
        m_InfoLabel.text = "";
        m_FullText = m_MetaData.GetHelpText();
        m_Timer = 0f;
        m_CurrentIndex = 0;

        m_UIController.StepButtonPressedEvent += m_FSM.SetHiddenState;
    }

    public override void Update(float deltaTime)
    {
        base.Update(deltaTime);

        if (m_CurrentIndex < m_FullText.Length)
        {
            m_Timer += deltaTime;
            if (m_Timer >= m_NewCharTime)
            {
                m_Timer -= m_NewCharTime;
                m_CurrentIndex++;
                while (m_CurrentIndex < m_FullText.Length && m_FullText[m_CurrentIndex] != ' ')
                    m_CurrentIndex++;
                m_InfoLabel.text = m_FullText.Substring(0, m_CurrentIndex);
            }
        }
    }

    public override void Exit()
    {
        base.Exit();

        m_InfoLabel.gameObject.SetActive(false);
        m_UIController.StepButtonPressedEvent -= m_FSM.SetHiddenState;
    }
}
