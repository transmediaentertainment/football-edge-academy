using TransTech.FiniteStateMachine;
using TransTech.Delegates;
using Holoville.HOTween;
using UnityEngine;

public class ActionListShowingState : ActionListState
{
    private AnimationSceneUIController m_Controller;
    private bool m_FirstTime = true;
    private const float m_StayUpTime = 4f;
    private float m_Timer;

    public event VoidDelegate TimerFinishedEvent;

    public ActionListShowingState(AnimationSceneUIController controller, ActionListUIFSMEngine fsm, UIPanel mainPanel, UIPanel smallPanel)
        : base(fsm, mainPanel, smallPanel)
    {
        m_Controller = controller;
        m_FirstTime = true;
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        HOTween.To(m_MainPanel.transform, ActionListUIFSMEngine.TweenDuration, new TweenParms().Prop("localPosition", m_FSM.MainPanelShowingPos).Ease(EaseType.EaseInOutQuad));
        HOTween.To(m_SmallPanel.transform, ActionListUIFSMEngine.TweenDuration, new TweenParms().Prop("localPosition", m_FSM.SmallPanelHidingPos).Ease(EaseType.EaseInOutQuad));

        m_Timer = 0f;

        m_Controller.ActionButtonPressedEvent += ResetTimer;
        AnimationSceneUIController.FallthroughDragEvent += m_FSM.SetHiddenStateWrapper;
        ActionPanelEvents.PanelDragEvent += ResetTimerVec2Wrapper;
    }

    public override void Exit()
    {
        base.Exit();

        m_Controller.ActionButtonPressedEvent -= ResetTimer;
        AnimationSceneUIController.FallthroughDragEvent -= m_FSM.SetHiddenStateWrapper;
        ActionPanelEvents.PanelDragEvent -= ResetTimerVec2Wrapper;
        m_FirstTime = false;
    }

    private void ResetTimerVec2Wrapper(Vector2 vec)
    {
        m_Timer = 0f;
    }

    private void ResetTimer(int buttonNum)
    {
        m_Timer = 0f;
    }

    public override void Update(float deltaTime)
    {
        base.Update(deltaTime);

        m_Timer += deltaTime;
        if (m_Timer >= (m_FirstTime ? 2f : m_StayUpTime))
        {
            if (TimerFinishedEvent != null)
                TimerFinishedEvent();
        }
    }
}
