using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class AnimControlStepInfoPauseState : AnimControlState 
{
    private MetaData m_MetaData;
    private UILabel m_Label;
    private AnimationControl m_AnimationControl;
    private EffectsControl m_EffectsControl;

    public AnimControlStepInfoPauseState(MetaData metaData, AnimationControl animationControl, AnimationSceneUIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton, UILabel label, EffectsControl effectsControl)
        : base(controller, fsm, playButton, pauseButton)
    {
        m_MetaData = metaData;
        m_Label = label;
        m_AnimationControl = animationControl;
        m_EffectsControl = effectsControl;
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        AnimationControlUIFSMEngine.EnableAndFadeButton(m_PauseButton);
        AnimationControlUIFSMEngine.DisableAndFadeButton(m_PlayButton);

        m_FSM.SetIconsVisibility(false, false, true);

        m_Controller.PlayButtonPressedEvent += PlayButtonPressed;
        m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
        m_Controller.StepButtonPressedEvent += StepButtonPressed;
        m_Controller.ShowInfoButtonPressedEvent += InfoButtonPressed;
        m_Controller.ActionButtonPressedEvent += ActionButtonPressed;

        SpeedControl.Instance.SetPaused();

        m_Label.gameObject.SetActive(true);
        m_Label.text = m_MetaData.GetTechniqueStep(m_AnimationControl.CurrentAnimation, m_AnimationControl.NormalizedTime);

        m_EffectsControl.TurnOnLineEffect();
    }

    public override void Exit()
    {
        base.Exit();

        m_Label.gameObject.SetActive(false);

        m_Controller.PlayButtonPressedEvent -= PlayButtonPressed;
        m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
        m_Controller.StepButtonPressedEvent -= StepButtonPressed;
        m_Controller.ShowInfoButtonPressedEvent -= InfoButtonPressed;
        m_Controller.ActionButtonPressedEvent -= ActionButtonPressed;

        m_EffectsControl.TurnOffLineEffect();
    }

    private void PlayButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Play);
    }

    private void SlowButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Slow);
    }

    private void StepButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.StepPlay);
    }

    private void InfoButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Play);
    }

    private void ActionButtonPressed(int button)
    {
        m_FSM.SetState(AnimControlStates.Play);
    }
}
