using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class AnimControlPlayState : AnimControlState 
{
    public AnimControlPlayState(AnimationSceneUIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton) : base(controller, fsm, playButton, pauseButton) { }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        AnimationControlUIFSMEngine.EnableAndFadeButton(m_PauseButton);
        AnimationControlUIFSMEngine.DisableAndFadeButton(m_PlayButton);

        m_FSM.SetIconsVisibility(false, false, false);

        m_Controller.PauseButtonPressedEvent += PauseButtonPressed;
        m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
        m_Controller.StepButtonPressedEvent += StepButtonPressed;

        SpeedControl.Instance.SetFullSpeed();
    }

    public override void Exit()
    {
        base.Exit();

        m_Controller.PauseButtonPressedEvent -= PauseButtonPressed;
        m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
        m_Controller.StepButtonPressedEvent -= StepButtonPressed;
    }

    private void PauseButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Pause);
    }

    private void SlowButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Slow);
    }

    private void StepButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.StepPlay);
    }
}
