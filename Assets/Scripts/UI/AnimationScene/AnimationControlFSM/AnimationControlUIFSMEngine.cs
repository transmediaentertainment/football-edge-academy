using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using Holoville.HOTween;

public enum AnimControlStates
{
    Play,
    Pause,
    Slow,
    SlowPause,
    StepPlay,
    StepPlayPause,
    StepInfoPause
}

public class AnimationControlUIFSMEngine : FSM
{
    private const float FadeDuration = 0.2f;

    //private AnimationSceneUIController m_Controller;
    private PlayStateUI m_PlayStateUI;

    private AnimControlPlayState m_PlayState;
    private AnimControlPauseState m_PauseState;
    private AnimControlSlowState m_SlowState;
    private AnimControlSlowPauseState m_SlowPauseState;
    private AnimControlStepPlayState m_StepPlayState;
    private AnimControlStepPlayPauseState m_StepPlayPauseState;
    private AnimControlStepInfoPauseState m_StepInfoPauseState;

    public AnimationControlUIFSMEngine(AnimationSceneUIController controller, MetaData metaData, AnimationControl animationControl, UIImageButton playButton, UIImageButton pauseButton, UIImageButton slowMoButton, UIImageButton stepButton, PlayStateUI playStateUI,
        UILabel stepInfoLabel, EffectsControl effectsControl)
    {
        //m_Controller = controller;
        m_PlayStateUI = playStateUI;

        m_PlayState = new AnimControlPlayState(controller, this, playButton, pauseButton);
        m_PauseState = new AnimControlPauseState(controller, this, playButton, pauseButton);
        m_SlowState = new AnimControlSlowState(controller, this, playButton, pauseButton);
        m_SlowPauseState = new AnimControlSlowPauseState(controller, this, playButton, pauseButton);
        m_StepPlayState = new AnimControlStepPlayState(metaData, animationControl, controller, this, playButton, pauseButton);
        m_StepPlayPauseState = new AnimControlStepPlayPauseState(controller, this, playButton, pauseButton);
        m_StepInfoPauseState = new AnimControlStepInfoPauseState(metaData, animationControl, controller, this, playButton, pauseButton, stepInfoLabel, effectsControl);

        // Register for events

        SetState(AnimControlStates.Play);
    }

    public void SetState(AnimControlStates state)
    {
        switch (state)
        {
            case AnimControlStates.Play:
                NewState(m_PlayState);
                break;
            case AnimControlStates.Pause:
                NewState(m_PauseState);
                break;
            case AnimControlStates.Slow:
                NewState(m_SlowState);
                break;
            case AnimControlStates.SlowPause:
                NewState(m_SlowPauseState);
                break;
            case AnimControlStates.StepInfoPause:
                NewState(m_StepInfoPauseState);
                break;
            case AnimControlStates.StepPlay:
                NewState(m_StepPlayState);
                break;
            case AnimControlStates.StepPlayPause:
                NewState(m_StepPlayPauseState);
                break;
            default:
                // Nothing
                break;
        }
    }

    public static void DisableAndFadeButton(UIImageButton button)
    {
        button.collider.enabled = false;
        var sprite = button.GetComponentInChildren<UISprite>();
        HOTween.To(sprite, FadeDuration, new TweenParms().Prop("color", Color.clear));
    }

    public static void EnableAndFadeButton(UIImageButton button)
    {
        button.collider.enabled = true;
        var sprite = button.GetComponentInChildren<UISprite>();
        HOTween.To(sprite, FadeDuration, new TweenParms().Prop("color", Color.white));
    }

    public void SetIconsVisibility(bool slowMo, bool paused, bool step)
    {
        SetIconEnabled(m_PlayStateUI.SlowMoSprite, m_PlayStateUI.SlowMoLabel, slowMo);
        SetIconEnabled(m_PlayStateUI.PausedSprite, m_PlayStateUI.PausedLabel, paused);
        SetIconEnabled(m_PlayStateUI.StepSprite, m_PlayStateUI.StepLabel, step);
    }

    private static void SetIconEnabled(UISprite sprite, UILabel label, bool enabled)
    {
        var col = sprite.color;
        col.a = enabled ? 1f : 0f;
        HOTween.To(sprite, FadeDuration, new TweenParms().Prop("color", col));
        HOTween.To(label, FadeDuration, new TweenParms().Prop("color", col));
    }
}
