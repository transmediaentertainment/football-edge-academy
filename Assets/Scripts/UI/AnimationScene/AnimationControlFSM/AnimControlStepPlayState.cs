using TransTech.FiniteStateMachine;
using TransTech.System.Debug;

public class AnimControlStepPlayState : AnimControlState
{
    private MetaData m_MetaData;
    private AnimationControl m_AnimationControl;
    private float m_NextTime;

    public AnimControlStepPlayState(MetaData metaData, AnimationControl animationControl, AnimationSceneUIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton)
        : base(controller, fsm, playButton, pauseButton)
    {
        m_MetaData = metaData;
        m_AnimationControl = animationControl;
    }

    public override void Enter(params object[] args)
    {
        base.Enter(args);

        AnimationControlUIFSMEngine.DisableAndFadeButton(m_PlayButton);
        AnimationControlUIFSMEngine.EnableAndFadeButton(m_PauseButton);

        m_FSM.SetIconsVisibility(false, false, true);

        m_Controller.PauseButtonPressedEvent += PauseButtonPressed;
        m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;

        SpeedControl.Instance.SetSlowMotion();

        // Find the next Time
        var currentTime = m_AnimationControl.NormalizedTime;
        bool done = false;
        int stepIndex = 0;
        m_NextTime = -1f;
        while (!done)
        {
            var instruction = m_MetaData.GetInstruction(m_AnimationControl.CurrentAnimation, stepIndex);
            if (instruction == null)
            {
                // No more steps so let it play out.
                done = true;
                continue;
            }
            if (instruction.Time > currentTime)
            {
                m_NextTime = instruction.Time;
                done = true;
            }
            stepIndex++;
        }
    }

    public override void Update(float deltaTime)
    {
        base.Update(deltaTime);

        // If we don't have another instruction, we just want to loop
        if (m_NextTime < 0f)
        {
            if (m_AnimationControl.NormalizedTime >= 0.99f || m_AnimationControl.NormalizedTime <= 0.03f)
                m_FSM.SetState(AnimControlStates.Play);
        }
        else if (m_AnimationControl.NormalizedTime >= m_NextTime)
        {
            m_FSM.SetState(AnimControlStates.StepInfoPause);
        }
    }

    public override void Exit()
    {
        base.Exit();

        m_Controller.PauseButtonPressedEvent -= PauseButtonPressed;
        m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
    }

    private void PauseButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.StepPlayPause);
    }

    private void SlowButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Slow);
    }
}
