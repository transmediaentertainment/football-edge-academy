using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class AnimControlPauseState : AnimControlState 
{
    public AnimControlPauseState(AnimationSceneUIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton) : base(controller, fsm, playButton, pauseButton) { }
   
    public override void Enter(params object[] args)
    {
        base.Enter(args);

        AnimationControlUIFSMEngine.DisableAndFadeButton(m_PauseButton);
        AnimationControlUIFSMEngine.EnableAndFadeButton(m_PlayButton);

        m_FSM.SetIconsVisibility(false, true, false);

        m_Controller.PlayButtonPressedEvent += PlayButtonPressed;
        m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
        m_Controller.StepButtonPressedEvent += StepButtonPressed;

        SpeedControl.Instance.SetPaused();
    }

    public override void Exit()
    {
        base.Exit();

        m_Controller.PlayButtonPressedEvent -= PlayButtonPressed;
        m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
        m_Controller.StepButtonPressedEvent -= StepButtonPressed;
    }

    private void PlayButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Play);
    }

    private void SlowButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.Slow);
    }

    private void StepButtonPressed()
    {
        m_FSM.SetState(AnimControlStates.StepPlay);
    }
}
