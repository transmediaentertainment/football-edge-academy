using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public abstract class AnimControlState : FSMState 
{
    protected AnimationSceneUIController m_Controller;
    protected AnimationControlUIFSMEngine m_FSM;
    protected UIImageButton m_PlayButton;
    protected UIImageButton m_PauseButton;

    public AnimControlState(AnimationSceneUIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton)
    {
        m_Controller = controller;
        m_FSM = fsm;
        m_PlayButton = playButton;
        m_PauseButton = pauseButton;
    }
}
