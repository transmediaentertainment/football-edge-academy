using UnityEngine;
using System.Collections;
using UnityEditor;
using FEA;
using FEA.Language;

public class LanguageSelection : Editor
{
    [MenuItem("Edit/FEA Language/English US")]
    public static void SetEnglishUS()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.EnglishUS);
    }
	
	[MenuItem("Edit/FEA Language/English UK")]
	public static void SetEnglishUK()
	{
		PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.EnglishUS);
	}

    [MenuItem("Edit/FEA Language/French")]
    public static void SetFrench()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.French);
    }

    [MenuItem("Edit/FEA Language/Spanish")]
    public static void SetSpanish()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.Spanish);
    }

    [MenuItem("Edit/FEA Language/Portugese")]
    public static void SetPortugese()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.Portugues);
    }
}
