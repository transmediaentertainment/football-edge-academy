namespace TransTech.Delegates
{
    public delegate void VoidDelegate();

    public delegate void FloatDelegate(float val);

    public delegate void IntDelegate(int val);

    public delegate void UIntDelegate(uint val);

    public delegate void BoolDelegate(bool val);

    public delegate void StringDelegate(string val);
	
	public delegate void IntStringDelegate(int val, string val2);

    public delegate void BoolIntDelegate(bool val, int val2);

    public delegate void BoolStringDelegate(bool val, string val2);

    public delegate void BoolStringArrayDelegate(bool val, string[] val2);
}